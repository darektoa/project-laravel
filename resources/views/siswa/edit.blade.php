<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Edit Data</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

  </head>
  <body style="background-color: #f1f1f1;">
    <!-- ini navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-danger">
    <a class="navbar-brand" href="">
    <img src="https://nua8nqpf6qzliamnzx7yba-on.drv.tw/Google Drive/Abdul Fattah/assets/logoRexensoft.svg" width="50" class="d-inline-block align-top" alt="">
    </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="">Rexensoft</a>
</nav>
    <!-- ini batas navbar -->
<br>
<!-- alert -->
   <div class="container">
     <h1>Edit Data Siswa</h1>
         @if(session('sukses'))
            <div class="alert alert-success" role="alert">
            {{session('sukses')}}
            </div>
         @endif
    </div>
<!-- alert -->
<br>
    <!-- bawah ni adalah form -->
        <div class="row">
            <div class="container">
                <form action="/update/{{$siswa->id}}" method="POST">
                         {{csrf_field()}}
                          <div class="form-group" >
                            <label for="exampleFormControlInput1">Nama Lengkap</label>
                            <input name="nama_lengkap" type="text" class="form-control" id="exampleFormControlInput1" placeholder="Isi Nama Lengkap" value="{{$siswa->nama_lengkap}}">
                          </div>
                          <div class="form-group">
                            <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                            <select name="jenis_kelamin" class="form-control" id="exampleFormControlSelect1" value="{{$siswa->jenis_kelamin}}">
                              <option value="L" @if($siswa->jenis_kelamin == 'L' ) selected @endif>Laki-Laki</option>
                              <option value="P" @if($siswa->jenis_kelamin == 'P' ) selected @endif>Perempuan</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="exampleFormControlSelect1">Agama</label>
                            <select name="agama" class="form-control" id="exampleFormControlSelect1" value="{{$siswa->agama}}">
                              <option @if($siswa->agama == 'Islam' ) selected @endif>Islam</option>
                              <option @if($siswa->agama == 'Non Muslim' ) selected @endif>Non Muslim</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="exampleFormControlTextarea1">Alamat</label>
                            <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="2" placeholder="Alamat Anda">{{$siswa->alamat}}</textarea>
                            <br>
                          <button type="submit" class="btn btn-success float-right">Update</button>
                          <a href="/" class="btn btn-secondary btn-md active" role="button" aria-pressed="true">Kembali</a>
                    </form>
                </div>
              </div>
    <!-- atas ni adalah form -->


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>

<!--Java Script  -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<!-- Akhir JavaScript -->
  </body>
</html>