<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Daftar Siswa</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
    
  </head>
  <body style="background-color: #f1f1f1;">
    <!-- ini navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-danger">
    <a class="navbar-brand" href="">
    <img src="https://nua8nqpf6qzliamnzx7yba-on.drv.tw/Google Drive/Abdul Fattah/assets/logoRexensoft.svg" width="50" class="d-inline-block align-top" alt="">
    </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="">Rexensoft</a>
</nav>
    <!-- ini batas navbar -->
<br>
   <div class="container">
   @if(session('sukses'))
        <div class="alert alert-success" role="alert">
        {{session('sukses')}}
      </div>
    @endif

    <div class="jumbotron jumbotron-fluid" style="background-color: #fff; border-radius: 1vw;">
      <div class="container">
        <div class="row">
        <!-- grid col data siswa -->
            <div class="col-12">
                <h1 style="padding: 0 15px; margin-bottom: 20px;display: flex; align-items: center; justify-content: space-between; font-size: 30px;">Daftar Murid RPL<a href="/tambah" class="btn btn-info btn-sm float-right" role="button" >Tambah Data Siswa</a></h1>
            </div>
       </div>
            <!-- table ke database -->
            <div class="container">
              <div class="row">
                <div class="col-12">
                  <table class="table table-borderless table-hover">
                      <thead class="thead table-info text-center">
                          <th>Nama Lengkap</th>
                          <th>Jenis Kelamin</th>
                          <th>Agama</th>
                          <th>Alamat</th>
                          <th>Tools</th>
                      </thead>
                      @foreach($data_siswa as $siswa)
                      <tr class="text-center">
                          <td>{{$siswa->nama_lengkap}}</td>
                          <td>{{$siswa->jenis_kelamin}}</td>
                          <td>{{$siswa->agama}}</td>
                          <td>{{$siswa->alamat}}</td>
                          <td style="display: flex; justify-content: space-around; align-items: center;">
                            <a href="/edit/{{$siswa->id}}" class=" btn btn-warning btn-sm"> Edit Data </a>
                            <a href="/delete/{{$siswa->id}}" class=" btn btn-danger btn-sm" onclick="return confirm ('Yakin Dihapus?')" > Delete </a>
                          </td>
                      </tr>
                      @endforeach
                  </table>
                </div>
              </div>
            </div>

      </div>
    </div>

            <!-- akhir table ke database -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>

<!--Java Script  -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<!-- Akhir JavaScript -->
  </body>
</html>