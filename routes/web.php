<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

route::GET('/', 'SiswaController@index');

route::GET('/edit/{id}', 'SiswaController@edit');

route::POST('/update/{id}', 'SiswaController@update');

route::POST('/create','SiswaController@create');

route::GET('/tambah','SiswaController@tambah');

route::GET('/delete/{id}', 'SiswaController@delete');